package com.epic.energyservices.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.epic.energyservices.exception.AppServiceException;
import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.repository.BusinessTypeRepository;
import com.epic.energyservices.repository.ClientRepository;
import com.epic.energyservices.repository.InvoiceRepository;
import com.epic.energyservices.repository.OperationalHeadquartersRepository;
import com.epic.energyservices.service.ClientService;

@Transactional
@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientRepository clientRepository;

	@Autowired
	InvoiceRepository invoiceRepository;


	@Autowired
	OperationalHeadquartersRepository operationalHeadquartersRepository;
	

	@Autowired
	CityServiceImpl cityServiceImpl;

	@Autowired
	BusinessTypeRepository businessTypeRepository;

	@Transactional
	@Override
	public Client save(Client client) {
		try {
			// controllo comune Sede Legale
			String registeredOfficeCityName = client.getRegisteredOffice().getCity().getName();
			if (cityServiceImpl.findByName(registeredOfficeCityName) != null) {
				client.getRegisteredOffice().setCity(cityServiceImpl.findByName(registeredOfficeCityName));
			} else {
				throw new AppServiceException();
			}
			/** calcolo PartitaIva **/
			// cerco id della provincia collegata al comune
			int provinceId = cityServiceImpl.findProvinceIdByCityName(registeredOfficeCityName);
			// creo il codice per provincia
			String provinceCode = provinceCode(provinceId);
			// calcolo la partita iva
			String partitaIva = partitaIvaCalc(randomNumber(), provinceCode);
			client.setPartitaIva(partitaIva);
			// set del tipo
			client.setBusinessType(
					businessTypeRepository.findByBusinessType(client.getBusinessType().getBusinessType()));
			return clientRepository.save(client);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Client update(Client client, int id) {
		try {
			Client c = clientRepository.findById(id).get();
			if (client.getBusinessName() != null)
				c.setBusinessName(client.getBusinessName());
			if (client.getEmail() != null)
				c.setEmail(client.getEmail());
			if (client.getPec() != null)
				c.setPec(client.getPec());
			if (!Objects.isNull(client.getPhone()))
				c.setPhone(client.getPhone());

			/**
			 * registered office
			 */
			if (client.getRegisteredOffice() != null) {
				if (client.getRegisteredOffice().getAdress() != null)
					c.getRegisteredOffice().setAdress((client.getRegisteredOffice().getAdress()));
				if (client.getRegisteredOffice().getStreetNumber() != null)
					c.getRegisteredOffice().setStreetNumber((client.getRegisteredOffice().getStreetNumber()));
				if (client.getRegisteredOffice().getLocality() != null)
					c.getRegisteredOffice().setLocality((client.getRegisteredOffice().getLocality()));
				if (!Objects.isNull(client.getRegisteredOffice().getPostalCode()))
					c.getRegisteredOffice().setPostalCode((client.getRegisteredOffice().getPostalCode()));
				if (client.getRegisteredOffice().getCity().getName() != null) {
					c.getRegisteredOffice()
							.setCity(cityServiceImpl.findByName(client.getRegisteredOffice().getCity().getName()));
				}
			}
			/**
			 * contact
			 */
			if (client.getContact() != null) {
				if (client.getContact().getContactEmail() != null)
					c.getContact().setContactEmail((client.getContact().getContactEmail()));
				if (client.getContact().getContactName() != null)
					c.getContact().setContactName((client.getContact().getContactName()));
				if (client.getContact().getContactSurname() != null)
					c.getContact().setContactSurname((client.getContact().getContactSurname()));
				if (!Objects.isNull(client.getContact().getContactPhone()))
					c.getContact().setContactPhone((client.getContact().getContactPhone()));
			}

			if (!Objects.isNull(client.getBusinessType())) {
				c.setBusinessType(
						businessTypeRepository.findByBusinessType(client.getBusinessType().getBusinessType()));
			}

			return clientRepository.save(c);
		} catch (Exception e) {
			throw new AppServiceException();
		}

	}

	@Override
	public String delete(int id) {
		try {
			Client c = clientRepository.findById(id).get();
			if (!invoiceRepository.findByClientId(id).isEmpty()) {
				invoiceRepository.deleteAll(invoiceRepository.findByClientId(id));
			}
			OperationalHeadquarters operationalHeadquarters = operationalHeadquartersRepository.findByClientId(id);

			clientRepository.delete(c);
			if (operationalHeadquarters != null) {
				operationalHeadquartersRepository.delete(operationalHeadquarters);
			}
			return "Deleted";
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	/**
	 * @return ordina per ultimo contatto riprende il metodo getLastContact(int
	 *         clientId)
	 */
	@Override
	public List<Entry<String, Date>> orderByLastContact() {
		try {
			Map<String, Date> lastContact = new TreeMap<>();
			for (Client c : findAll()) {
				lastContact.put(c.toString(), getLastContact(c.getId()));
			}
			List<Entry<String, Date>> list = new ArrayList<>(lastContact.entrySet());
			list.sort(Entry.comparingByValue());
			return list;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> orderByBusinessName(Pageable page) {
		try {
			return clientRepository.orderByBusinessName(page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> orderByCreatedAt(Pageable page) {
		try {
			return clientRepository.orderByCreatedAt(page);
		} catch (Exception e) {
			throw new AppServiceException();
		}
	}

	@Override
	public Page<Client> orderByRegisteredOffice(Pageable page) {
		try {
			return clientRepository.orderByRegisteredOffice(page);
		} catch (Exception e) {
			throw new AppServiceException();
		}

	}

	@Override
	public Page<Client> findAllClientsPageSort(Integer page, Integer size, String dir, String sort) {
		try {
			Pageable paging = PageRequest.of(page, size, Sort.Direction.fromString(dir), sort);
			Page<Client> pagedResult = clientRepository.findAll(paging);
			if (pagedResult.hasContent()) {
				return pagedResult;
			}
		} catch (Exception e) {
			throw new AppServiceException();
		}
		return null;
	}

	@Override
	public Page<Client> findByBusinessTypeBusinessType(String BusinessType, Pageable page) {
		try {
			return (clientRepository.findByBusinessTypeBusinessType(BusinessType.toUpperCase(), page));
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Client> filterByProvince(String province, Pageable page) {
		try {
			return clientRepository.filterByProvince(province, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Client findById(int clientId) {
		try {
			return clientRepository.findById(clientId).get();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public Page<Client> filterByBusinessName(String businessName, Pageable page) {
		try {
			return clientRepository.filterByBusinessName(businessName, page);
		} catch (Exception e) {
			throw new AppServiceException();
		}
	}

	@Override
	public Page<Client> filterByCreatedAt(Date startDate, Date endDate, Pageable page) {
		try {
			return clientRepository.filterByCreatedAt(startDate, endDate, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Date getLastContact(int clientId) {
		try {
			Client c = clientRepository.findById(clientId).get();
			List<Invoice> invoices = invoiceRepository.findByClientId(clientId);
			// se ci sono fatture
			if (!invoices.isEmpty()) {
				return invoices.stream().map(Invoice::getCreatedAt).max(Date::compareTo).get();
			}
			// se lista vuota, metti data inserimento
			return c.getCreatedAt();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Date getCreatedAt(int clientId) {
		try {
			return clientRepository.getCreatedAt(clientId);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	// PARTITA IVA

	@Override
	public int randomNumber() {
		try {
			return (int) (Math.random() * 999999) + 1000000;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public String provinceCode(int provinceId) {
		try {
			String provinceCode = "";
			String id = provinceId + "";
			if (id.length() == 1) {
				provinceCode = "00" + id;
			} else if (id.length() == 2) {
				provinceCode = "0" + id;
			} else if (id.length() == 3) {
				provinceCode = id;

			}
			return provinceCode;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public String partitaIvaCalc(int randomNumber, String provinceCode) {
		try {
			int even = 0;
			int odd = 0;
			int controlCode = 0;
			// unisco i valori in una stringa
			String firstPartString = randomNumber + provinceCode;
			// divido in array di caratteri
			char[] array = firstPartString.toCharArray();
			// somma dei numeri in posizione pari
			for (int i = 0; i < array.length; i++) {
				if (i % 2 == 0) {
					int n = Character.getNumericValue(array[i]);
					even += n;
				}
			}
			// somma dei numeri in posizione dispari
			for (int i = 0; i < array.length; i++) {
				if ((i % 2) != 0) {
					int n = Character.getNumericValue(array[i]);
					int oddMultiplication = n * 2;
					if (oddMultiplication > 9) {
						oddMultiplication -= 9;
					}
					odd += oddMultiplication;
				}
			}
			// somma generale
			int totalEvenOdd = even + odd;

			// codice di controllo
			// se somma minore di 10 trovo la differenza
			if (totalEvenOdd < 10) {
				controlCode = 10 - totalEvenOdd;
			} else {
				// se somma maggiore di 10 prendo solo ultima cifra
				String totalEvenOddString = totalEvenOdd + "";
				char controlCodeChar = totalEvenOddString.charAt(totalEvenOddString.length() - 1);
				int controlCodeC = Character.getNumericValue(controlCodeChar);
				controlCode = 10 - controlCodeC;

			}
			String partitaIvaString = "IT" + randomNumber + provinceCode + controlCode;
			return partitaIvaString;
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	/**
	 * per impaginare
	 */
	@Override
	public Page<Client> findPaginated(int pageNumber, int pageSize, String sortField, String sortDirection) {
		try {
			Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
					: Sort.by(sortField).descending();
			Pageable pageable = PageRequest.of(pageNumber - 1, pageSize, sort);
			return this.clientRepository.findAll(pageable);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public List<Client> findAll() {
		try {
			return clientRepository.findAll();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

}
