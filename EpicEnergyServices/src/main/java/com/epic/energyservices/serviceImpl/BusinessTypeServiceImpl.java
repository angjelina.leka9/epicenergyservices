package com.epic.energyservices.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epic.energyservices.exception.AppServiceException;
import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.repository.BusinessTypeRepository;
import com.epic.energyservices.service.BusinessTypeService;

@Service
public class BusinessTypeServiceImpl implements BusinessTypeService {

	@Autowired
	BusinessTypeRepository businessTypeRepository;
	
	@Autowired
	ClientServiceImpl clientServiceImpl;

	@Override
	public BusinessType addBusinessType(BusinessType businessType) {
		try {
			return businessTypeRepository.save(businessType);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public BusinessType update(BusinessType businessType, int id) {
		try {
			BusinessType b = businessTypeRepository.findById(id).get();
			if(businessType != null) {
				if(businessType.getBusinessType() != null)
					b.setBusinessType(businessType.getBusinessType());
			}
			return businessTypeRepository.save(b);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	
	}

}
