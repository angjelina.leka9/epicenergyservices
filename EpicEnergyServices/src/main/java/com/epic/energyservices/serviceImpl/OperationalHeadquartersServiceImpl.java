package com.epic.energyservices.serviceImpl;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epic.energyservices.exception.AppServiceException;
import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.repository.CityRepository;
import com.epic.energyservices.repository.OperationalHeadquartersRepository;
import com.epic.energyservices.service.OperationalHeadquartersService;

@Transactional
@Service
public class OperationalHeadquartersServiceImpl implements OperationalHeadquartersService {

	@Autowired
	ClientServiceImpl clientServiceImpl;

	@Autowired
	OperationalHeadquartersRepository operationalHeadquartersRepository;

	@Autowired
	CityRepository cityRepository;

	@Override
	public OperationalHeadquarters save(OperationalHeadquarters operationalHeadquarters, int clientId) {
		try {
			if (findByClientId(clientId) != null) {
				throw new AppServiceException();
			}
			operationalHeadquarters.setClient(clientServiceImpl.findById(clientId));
			String operationalHeadquartersCityName = operationalHeadquarters.getCity().getName();
			if (cityRepository.findByName(operationalHeadquartersCityName).isPresent()) {
				operationalHeadquarters.setCity(cityRepository.findByName(operationalHeadquartersCityName).get());
			}
			return operationalHeadquartersRepository.save(operationalHeadquarters);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public OperationalHeadquarters update(OperationalHeadquarters operationalHeadquarters, int id) {
		try {
			OperationalHeadquarters oh = operationalHeadquartersRepository.findById(id).get();
			if (operationalHeadquarters != null) {
				if (operationalHeadquarters.getAdress() != null)
					oh.setAdress((operationalHeadquarters.getAdress()));
				if (operationalHeadquarters.getStreetNumber() != null)
					oh.setStreetNumber((operationalHeadquarters.getStreetNumber()));
				if (operationalHeadquarters.getLocality() != null)
					oh.setLocality((operationalHeadquarters.getLocality()));
				if (!Objects.isNull(operationalHeadquarters.getPostalCode()))
					oh.setPostalCode((operationalHeadquarters.getPostalCode()));
				if (operationalHeadquarters.getCity() != null) {
					if (operationalHeadquarters.getCity().getName() != null) {
						oh.setCity(cityRepository.findByName(operationalHeadquarters.getCity().getName()).get());
					}
				}
			}
			return operationalHeadquartersRepository.save(oh);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public String delete(int id) {
		try {
			OperationalHeadquarters oh = operationalHeadquartersRepository.findById(id).get();
			oh.setClient(null);
			operationalHeadquartersRepository.delete(operationalHeadquartersRepository.findById(id).get());
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
		return "Sede operativa eliminata";
	}

	@Override
	public OperationalHeadquarters findByClientId(int clientId) {
		try {
			return operationalHeadquartersRepository.findByClientId(clientId);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public OperationalHeadquarters filterByProvince(String province) {
		try {
			return operationalHeadquartersRepository.filterByProvince(province);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public OperationalHeadquarters findById(int id) {
		try {
			return operationalHeadquartersRepository.findById(id).get();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<OperationalHeadquarters> findAll() {
		try {
			return operationalHeadquartersRepository.findAll();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public void delete(OperationalHeadquarters operationalHeadquarters) {
		try {
			operationalHeadquartersRepository.delete(operationalHeadquarters);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}
}
