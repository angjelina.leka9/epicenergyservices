package com.epic.energyservices.serviceImpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.epic.energyservices.exception.AppServiceException;
import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.repository.ClientRepository;
import com.epic.energyservices.repository.InvoiceRepository;
import com.epic.energyservices.repository.InvoiceStatusRepository;
import com.epic.energyservices.service.InvoiceService;

@Transactional
@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	InvoiceRepository invoiceRepository;

	@Autowired
	ClientServiceImpl clientServiceImpl;

	@Autowired
	InvoiceStatusRepository invoiceStatusRepository;
	
	@Autowired
	ClientRepository clientRepository;

	@Override
	public Invoice save(Invoice invoice, int clientId) {
		try {
			Client c = clientRepository.findById(clientId).get();
			invoice.setClient(c);

			// setto il numero della fattura
			int size = invoiceRepository.findByClientId(clientId).size();
			if (size != 0) {
				int lastInvoiceNumber = invoiceRepository.findByClientId(clientId).get(size - 1).getNumber();
				invoice.setNumber(lastInvoiceNumber + 1);
			} else {
				invoice.setNumber(1);
			}
			// stato
			invoice.setInvoiceStatus(invoiceStatusRepository
					.findByInvoiceStatus(invoice.getInvoiceStatus().getInvoiceStatus().toUpperCase()));

			return invoiceRepository.save(invoice);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Invoice update(Invoice invoice, int clientId) {
		try {
			Invoice f = invoiceRepository.findByNumberAndClientId(invoice.getNumber(), clientId);
			if (f != null) {
				if (!Objects.isNull(invoice.getAmount())) {
					f.setAmount(invoice.getAmount());
				}
				if (!Objects.isNull(invoice.getNumber()))
					f.setNumber(invoice.getNumber());
				f.setInvoiceStatus(
						invoiceStatusRepository.findByInvoiceStatus(invoice.getInvoiceStatus().getInvoiceStatus()));
			}
			return invoiceRepository.save(f);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public String delete(int number, int clientId) {
		try {
			Invoice invoice = invoiceRepository.findByNumberAndClientId(number, clientId);
			invoiceRepository.delete(invoice);
			return "Deleted";
		} catch (Exception e) {
			throw new AppServiceException(e);
		}

	}

	@Override
	public void deleteAll(int clientId) {
		try {
			invoiceRepository.deleteAll();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return lista fatture per cliente
	 */
	@Override
	public Page<Invoice> findByClientId(int clientId, Pageable page) {
		try {
			return invoiceRepository.findByClientId(clientId, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatturato annuale del cliente per l'anno scelto
	 */
	@Override
	public BigDecimal getSum(int clientId, int year) {
		try {
			return invoiceRepository.getSum(clientId, year);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatturato annuale dei clienti per l'anno scelto
	 */
	@Override
	public Page<List<Map<Client, BigDecimal>>> getAmountByYear(int year, Pageable page) {
		try {
			return invoiceRepository.getAmountByYear(year, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatture per range importo
	 */
	@Override
	public Page<Invoice> findByAmountRange(BigDecimal min, BigDecimal max, Pageable page) {
		try {
			return new PageImpl<>(invoiceRepository.findByAmountRange(min, max));
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatture per range importo per anno scelto
	 */
	@Override
	public Page<Invoice> findByAmountRangePerYear(int year, BigDecimal min, BigDecimal max, Pageable page) {
		try {
			return invoiceRepository.findByAmountRangePerYear(year, min, max, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatture con importi maggiori di un certo valore per l'anno
	 *         selezionato
	 */
	@Override
	public Page<Invoice> findByAmountGreaterThanPerYear(int year, BigDecimal base, Pageable page) {
		try {
			return invoiceRepository.findByAmountGreaterThanPerYear(year, base, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return importi minori di un certo valore per l'anno selezionato
	 */
	@Override
	public Page<Invoice> findByAmountLessThanPerYear(int year, BigDecimal base, Pageable page) {
		try {
			return invoiceRepository.findByAmountLessThanPerYear(year, base, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return tutte le fatture per l'anno selezionato
	 */
	@Override
	public Page<Invoice> findAllInvoiceByYear(int year, Pageable page) {
		try {
			return invoiceRepository.findAllInvoiceByYear(year, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatture per stato
	 */
	@Override
	public Page<Invoice> findByInvoiceStatusInvoiceStatus(String invoiceStatus, Pageable page) {
		try {
			return invoiceRepository.findByInvoiceStatusInvoiceStatus(invoiceStatus.toUpperCase(), page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatturato annuale maggiore di per anno
	 */
	@Override
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountGreaterThanPerYear(int year, BigDecimal base,
			Pageable page) {
		try {
			return invoiceRepository.findByTotalAmountGreaterThanPerYear(year, base, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatturato annuale minore di per anno
	 */
	@Override
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountLessThanPerYear(int year, BigDecimal base,
			Pageable page) {
		try {
			return invoiceRepository.findByTotalAmountLessThanPerYear(year, base, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fatturato annuale range per anno
	 */
	@Override
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountRangePerYear(int year, BigDecimal min, BigDecimal max,
			Pageable page) {
		try {
			return invoiceRepository.findByTotalAmountRangePerYear(year, min, max, page);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return fattura per numero e cliente
	 */
	@Override
	public Invoice findByNumberAndClientId(int number, int clientId) {
		try {
			return invoiceRepository.findByNumberAndClientId(number, clientId);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	/**
	 * @return lista fattura per cliente id serve per metodo save
	 */
	@Override
	public List<Invoice> findByClientId(int clientId) {
		try {
			return invoiceRepository.findByClientId(clientId);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Invoice findById(int id) {
		try {
			return invoiceRepository.findById(id).get();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public List<Invoice> findAll() {
		try {
			return invoiceRepository.findAll();
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	public void deleteAll(List<Invoice> list) {
		try {
			invoiceRepository.deleteAll(list);
		} catch (Exception e) {
			throw new AppServiceException(e);
		}
	}

	@Override
	public Page<Invoice> getAmountClientPerYear(int clientId, int year, Pageable page) {
		return invoiceRepository.getAmountClientPerYear(clientId, year, page);
		 
	}

}
