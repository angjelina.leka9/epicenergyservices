package com.epic.energyservices.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQuery(name = "Invoice.getSum", query = "SELECT SUM(i.amount) FROM Invoice i WHERE i.client.id=:clientId AND YEAR(i.createdAt)=:year")
@NamedQuery(name = "Invoice.getAmountClientPerYear", query = "SELECT i FROM Invoice i WHERE i.client.id=:clientId AND YEAR(i.createdAt)=:year")
@NamedQuery(name = "Invoice.getAmountByYear", query = "SELECT i.client.id as client, SUM(i.amount) as amount FROM Invoice i WHERE YEAR(i.createdAt)= :year GROUP BY client ORDER BY amount")
@NamedQuery(name = "Invoice.findByAmountRange", query = "SELECT i FROM Invoice i WHERE i.amount BETWEEN :min AND :max")
@NamedQuery(name = "Invoice.findAllInvoiceByYear", query = "SELECT i FROM Invoice i WHERE YEAR(i.createdAt)=:year")
@NamedQuery(name = "Invoice.findByAmountRangePerYear", query = "SELECT i FROM Invoice i WHERE YEAR(i.createdAt)=:year AND i.amount BETWEEN :min AND :max")
@NamedQuery(name = "Invoice.findByAmountGreaterThanPerYear", query = "SELECT i FROM Invoice i WHERE YEAR(i.createdAt)=:year AND i.amount > :base")
@NamedQuery(name = "Invoice.findByAmountLessThanPerYear", query = "SELECT i FROM Invoice i WHERE YEAR(i.createdAt)=:year AND i.amount < :base")
@NamedQuery(name = "Invoice.findByTotalAmountGreaterThanPerYear", query = "SELECT i.client.id as client, SUM(i.amount) as amount FROM Invoice i WHERE YEAR(i.createdAt)=:year GROUP BY client HAVING SUM(i.amount) >:base")
@NamedQuery(name = "Invoice.findByTotalAmountLessThanPerYear", query = "SELECT i.client.id as client, SUM(i.amount) as amount FROM Invoice i WHERE YEAR(i.createdAt)=:year GROUP BY client HAVING SUM(i.amount) <:base")
@NamedQuery(name = "Invoice.findByTotalAmountRangePerYear", query = "SELECT i.client.id as client, SUM(i.amount) as amount FROM Invoice i WHERE YEAR(i.createdAt)=:year GROUP BY client HAVING SUM(i.amount) BETWEEN :min AND :max")

public class Invoice extends BaseEntity {

	/**
	 * Importo.
	 */
	@Column(nullable = false, length = 125)
	private BigDecimal amount;
	/**
	 * Numero fattura. Viene impostato automaticamente. Si dovrà inserire solo per
	 * la modifica ed eliminazione della fattura. Ogni cliente parte dalla fattura
	 * numero 1. Il numero cresce in base alle fatture per cliente.
	 */
	private int number;
	/**
	 * Client.
	 */
	@ManyToOne
	private Client client;
	/**
	 * Stato della fattura. Preinserire gli stati nel DB prima della'inizio della
	 * creazione di fatture. Lo stato può essere scelto solo tra quelli presenti nel
	 * DB.
	 */
	@ManyToOne
	private InvoiceStatus invoiceStatus;

}
