package com.epic.energyservices.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Tipi di Client.
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class BusinessType extends BaseEntity {
	/**
	 * Tipi di Client. Devono essere preinseriti nel DB. Quando viene creato un
	 * cliente, si sceglie tra i tipi disponibili
	 */
	@Column(length = 40, nullable = false)
	private String businessType;

	/**
	 * Esempio:
	 * 
	 * PA SAS SPA SRL SAPA
	 */
}
