package com.epic.energyservices.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * Dati di contatto.
 *
 */
@Data
@Embeddable
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Contact {
	/**
	 * Email contatto.
	 */
	@Column(nullable = false, length = 125)
	private String contactEmail;
	/**
	 * Nome contatto.
	 */
	@Column(nullable = false, length = 125)
	private String contactName;
	/**
	 * Cognoome contatto.
	 */
	@Column(nullable = false, length = 125)
	private String contactSurname;
	/**
	 * Telefono contatto.
	 */
	@Column(nullable = false, length = 125)
	private long contactPhone;
}
