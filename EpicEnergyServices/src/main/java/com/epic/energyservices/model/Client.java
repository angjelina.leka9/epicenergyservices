package com.epic.energyservices.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * Classe Client
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQuery(name = "Client.orderByBusinessName", query = "SELECT c FROM Client c ORDER BY businessName")
@NamedQuery(name = "Client.orderByCreatedAt", query = "SELECT c FROM Client c ORDER BY createdAt")
@NamedQuery(name = "Client.orderByRegisteredOffice", query = "SELECT c FROM Client c ORDER BY registeredOffice.city.province.name")
@NamedQuery(name = "Client.filterByCreatedAt", query = "SELECT c FROM Client c WHERE c.createdAt BETWEEN :startDate AND :endDate ORDER BY c.createdAt")
@NamedQuery(name = "Client.filterByBusinessName", query = "SELECT c FROM Client c WHERE c.businessName LIKE CONCAT(CONCAT('%', :businessName), '%')")
@NamedQuery(name = "Client.getCreatedAt", query = "SELECT c.createdAt FROM Client c WHERE c.id =:clientId")
@NamedQuery(name = "Client.filterByProvince", query = "SELECT c FROM Client c WHERE c.registeredOffice.city.province.name LIKE CONCAT(CONCAT('%', :province), '%')")
public class Client extends BaseEntity {
	/**
	 * Nome azienda
	 */
	@Column(nullable = false, length = 125)
	private String businessName;
	/**
	 * Partita Iva La partita iva viene generata automaticamente.
	 */
	@Column(length = 13, unique = true)
	private String partitaIva;
	/**
	 * Email
	 */
	@Column(nullable = false, length = 125)
	private String email;
	/**
	 * Pec
	 */
	@Column(nullable = false, length = 125)
	private String pec;
	/**
	 * Numero telefono
	 */
	@Column(nullable = false, length = 15)
	private long phone;

	/**
	 * Sede legale
	 */
	@Embedded
	private RegisteredOffice registeredOffice;

	/**
	 * Dati di contatto
	 */
	@Embedded
	private Contact contact;

	/**
	 * Tipo di azienda
	 */
	@ManyToOne
	private BusinessType businessType;

}
