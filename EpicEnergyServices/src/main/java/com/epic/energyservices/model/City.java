package com.epic.energyservices.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class City extends BaseEntity {

	/**
	 * Nome del comune.
	 */
	@Column(nullable = false, length = 125)
	private String name;

	@ManyToOne
	private Province province;

}
