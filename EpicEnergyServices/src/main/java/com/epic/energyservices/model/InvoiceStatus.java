package com.epic.energyservices.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * Stato della fattura.
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class InvoiceStatus extends BaseEntity {
	@Column(nullable = false, length = 40)
	private String invoiceStatus;
	/**
	 * Esempio:
	 * 
	 * EMESSA PAGATA ..
	 * 
	 * salvare in upperCase
	 */
}
