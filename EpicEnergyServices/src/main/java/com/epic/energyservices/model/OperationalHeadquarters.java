package com.epic.energyservices.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 
 * Sede Operativa.
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@NamedQuery(name = "OperationalHeadquarters.filterByProvince", query = "SELECT o FROM OperationalHeadquarters o WHERE o.city.province.name LIKE CONCAT(CONCAT('%', :province), '%')")
public class OperationalHeadquarters extends BaseEntity {

	/**
	 * Indirizzo.
	 */
	@Column(nullable = false, length = 125)
	private String adress;
	/**
	 * Numero civico.
	 */
	@Column(nullable = false)
	private String streetNumber;
	/**
	 * Località.
	 */
	@Column(nullable = false, length = 125)
	private String locality;
	/**
	 * Codice postale.
	 */
	@Column(length = 5)
	private int postalCode;
	/**
	 * Comune.
	 */
	@ManyToOne
	private City city;
	/**
	 * Client.
	 */
	@OneToOne
	private Client client;

}
