package com.epic.energyservices.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisteredOffice {
	/**
	 * Indirizzo.
	 */
	@Column(nullable = false, length = 125)
	private String adress;
	/**
	 * Numero civico.
	 */
	@Column(nullable = false)
	private String streetNumber;
	/**
	 * Località.
	 */
	@Column(nullable = false, length = 125)
	private String locality;
	/**
	 * Codice postale.
	 */
	@Column(nullable = false, length = 5)
	private int postalCode;
	/**
	 * Comune.
	 */
	@ManyToOne
	private City city;

}
