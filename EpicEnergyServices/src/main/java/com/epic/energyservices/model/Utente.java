package com.epic.energyservices.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.epic.energyservices.security.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString

public class Utente extends BaseEntity {
	/**
	 * Username utente.
	 */
	@Column(length = 20, nullable = false)
	private String username;
	/**
	 * Email.
	 */
	@Column(length = 112, nullable = false, unique = true)
	private String email;
	/**
	 * Password (Lunghezza password codificata con BCrypt).
	 */
	@Column(length = 60, nullable = false)
	@JsonIgnore
	// @Convert(converter = Convertitore.class)
	private String password;
	/**
	 * Username utente.
	 */
	@Column(length = 20, nullable = false)
	private String name;
	/**
	 * Username utente.
	 */
	@Column(length = 20, nullable = false)
	private String surname;

	/**
	 * Ruoli di appartenenza.
	 */
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "utente_role", joinColumns = @JoinColumn(name = "utente_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Set<Role> role = new HashSet<Role>();

}
