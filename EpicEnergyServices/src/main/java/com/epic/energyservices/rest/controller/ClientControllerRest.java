package com.epic.energyservices.rest.controller;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.Client;
import com.epic.energyservices.serviceImpl.ClientServiceImpl;

@RestController
@RequestMapping("/rest/client")
public class ClientControllerRest {

	@Autowired
	ClientServiceImpl clientServiceImpl;

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/save")
	public Client save(@RequestBody Client client) {
		return clientServiceImpl.save(client);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/update") 
	public Client update(@RequestBody Client client, @RequestParam int clientId) {
		return clientServiceImpl.update(client, clientId);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/delete")
	public String delete(@RequestParam int clientId) {
		return clientServiceImpl.delete(clientId);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/orderByBusinessName")
	public Page<Client> orderByBusinessName(Pageable page) {
		return clientServiceImpl.orderByBusinessName(page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/orderByLastContact")
	public List<Entry<String, Date>> orderByLastContact() {
		return clientServiceImpl.orderByLastContact();
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/orderByCreatedAt")
	public Page<Client> orderByCreatedAt(Pageable page) {
		return clientServiceImpl.orderByCreatedAt(page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/orderByRegisteredOffice")
	public Page<Client> orderByRegisteredOffice(Pageable page) {
		return clientServiceImpl.orderByRegisteredOffice(page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping(value = "/findAllClientsPageSort")
	public Page<Client> findAllClientiPageSort(@RequestParam(defaultValue = "0") Integer page,
			@RequestParam(defaultValue = "3") Integer size, @RequestParam(defaultValue = "asc") String dir,
			@RequestParam(defaultValue = "id") String sort) {
		return clientServiceImpl.findAllClientsPageSort(page, size, dir, sort);

	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/filterByBusinessName")
	public Page<Client> filterByBusinessName(@RequestParam String businessName, Pageable page) {
		return clientServiceImpl.filterByBusinessName(businessName, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/filterByCreatedAt")
	public Page<Client> filterByCreatedAt(@RequestParam Date startDate, Date endDate, Pageable page) {
		return clientServiceImpl.filterByCreatedAt(startDate, endDate, page);
	}

	@GetMapping("/filterByProvince")
//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")	
	public Page<Client> filterByProvince(@RequestParam String province, Pageable page) {
		return clientServiceImpl.filterByProvince(province, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/getLastContact")
	public Date getLastContact(@RequestParam int clientId) {
		return clientServiceImpl.getLastContact(clientId);
	}


//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/getCreatedAt")
	public Date getCreatedAt(@RequestParam int clientId) {
		return clientServiceImpl.getCreatedAt(clientId);
	}

}
