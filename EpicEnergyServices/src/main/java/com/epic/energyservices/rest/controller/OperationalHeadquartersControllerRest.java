package com.epic.energyservices.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.serviceImpl.OperationalHeadquartersServiceImpl;

@RestController
@RequestMapping("/rest/operationalHeadquartes")
public class OperationalHeadquartersControllerRest {

	@Autowired
	OperationalHeadquartersServiceImpl operationalHeadquartersServiceImpl;
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/save")
	public OperationalHeadquarters save(@RequestBody OperationalHeadquarters operationalHeadquarters, @RequestParam int clientId) {
		return operationalHeadquartersServiceImpl.save(operationalHeadquarters, clientId);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/update") 
	public OperationalHeadquarters update(@RequestBody OperationalHeadquarters operationalHeadquarters, @RequestParam int clientId) {
		return operationalHeadquartersServiceImpl.update(operationalHeadquarters, clientId);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/delete") 
	public String delete(@RequestParam int id) {
		return operationalHeadquartersServiceImpl.delete(id);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByClientId") 
	public OperationalHeadquarters findByClientId(@RequestParam int clientId) {
		return operationalHeadquartersServiceImpl.findByClientId(clientId);
	} 
	
	
}
