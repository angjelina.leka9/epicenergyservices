package com.epic.energyservices.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.Utente;
import com.epic.energyservices.serviceImpl.UtenteServiceImpl;


@CrossOrigin(origins = "http://domain2.com", maxAge=3600)
@RestController
@RequestMapping("/rest/utente")
public class UtenteControllerRest {
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	UtenteServiceImpl utenteServiceImpl;
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/save")
	public Utente save(@RequestBody Utente utente) {
		return utenteServiceImpl.save(utente);
	}
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/delete")
	public String delete(@RequestParam int utenteId) {
		return utenteServiceImpl.delete(utenteId);
	}
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/update")
	public Utente update(@RequestBody Utente utente, int id) {
		return utenteServiceImpl.update(utente, id);
	}

}
