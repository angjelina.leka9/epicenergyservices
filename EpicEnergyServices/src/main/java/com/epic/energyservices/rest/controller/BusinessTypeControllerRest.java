package com.epic.energyservices.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.serviceImpl.BusinessTypeServiceImpl;

@RestController
@RequestMapping("/rest/businessType")
public class BusinessTypeControllerRest {

	@Autowired
	BusinessTypeServiceImpl businessTypeServiceImpl;

	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/addBusinessType")
	public BusinessType addBusinessType(@RequestBody BusinessType businessType) {
		return businessTypeServiceImpl.addBusinessType(businessType);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/updateBusinessType")
	public BusinessType update(@RequestBody BusinessType businessType, @RequestParam int id) {
		return businessTypeServiceImpl.update(businessType, id);
	}
	

}
