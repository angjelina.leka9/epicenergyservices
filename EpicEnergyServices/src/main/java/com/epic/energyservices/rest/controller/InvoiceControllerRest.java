package com.epic.energyservices.rest.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.serviceImpl.InvoiceServiceImpl;

@RestController
@RequestMapping("/rest/invoice")
public class InvoiceControllerRest {

	@Autowired
	InvoiceServiceImpl invoiceServiceImpl;

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/save")
	public Invoice save(@RequestBody Invoice invoice, @RequestParam int clientId) {
		return invoiceServiceImpl.save(invoice, clientId);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/update")
	public Invoice update(@RequestBody Invoice invoice, @RequestParam int clientId) {
		return invoiceServiceImpl.update(invoice, clientId);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/delete")
	public String delete(@RequestParam int number, int clientId) {
		return invoiceServiceImpl.delete(number, clientId);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/deleteAll")
	public void deleteAll(@RequestParam int clientId) {
		invoiceServiceImpl.deleteAll(clientId);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByClientId")
	public Page<Invoice> findByClientId(@RequestParam int clientId, Pageable page) {
		return invoiceServiceImpl.findByClientId(clientId, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public BigDecimal getSum(int clientId, int year) {
		return invoiceServiceImpl.getSum(clientId, year);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/getAmountByYear")
	public Page<List<Map<Client, BigDecimal>>> getAmountByYear(@RequestParam int year, Pageable page) {
		return invoiceServiceImpl.getAmountByYear(year, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByAmountRange")
	public Page<Invoice> findByAmountRange(@RequestParam BigDecimal amountMin, BigDecimal amountMax, Pageable page) {
		return invoiceServiceImpl.findByAmountRange(amountMin, amountMax, page);
	}

///	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")//
	@GetMapping("/findByAmountRangePerYear")
	public Page<Invoice> findByAmountRangePerYear(int year, BigDecimal min, BigDecimal max, Pageable page) {
		return invoiceServiceImpl.findByAmountRangePerYear(year, min, max, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByAmountGreaterThanPerYear")
	public Page<Invoice> findByAmountGreaterThanPerYear(int year, BigDecimal base, Pageable page) {
		return invoiceServiceImpl.findByAmountGreaterThanPerYear(year, base, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByAmountLessThanPerYear")
	public Page<Invoice> findByAmountLessThanPerYear(int year, BigDecimal base, Pageable page) {
		return invoiceServiceImpl.findByAmountLessThanPerYear(year, base, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByYear")
	public Page<Invoice> findByYear(@RequestParam int year, Pageable page) {
		return invoiceServiceImpl.findAllInvoiceByYear(year, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByInvoiceStatus")//
	public Page<Invoice> findByInvoiceStatus(@RequestBody String invoiceStatus, Pageable page) {
		return invoiceServiceImpl.findByInvoiceStatusInvoiceStatus(invoiceStatus, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByYearAndClientId")//
	public Page<Invoice> findByYearAndClientId(@RequestParam int year, int clientId, Pageable page) {
		return invoiceServiceImpl.getAmountClientPerYear(year, clientId, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByTotalAmountGreaterThanPerYear")
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountGreaterThanPerYear(int year, BigDecimal base,
			Pageable page) {
		return invoiceServiceImpl.findByTotalAmountGreaterThanPerYear(year, base, page);
	}

//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	@GetMapping("/findByTotalAmountLessThanPerYear")
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountLessThanPerYear(int year, BigDecimal base,
			Pageable page) {
		return invoiceServiceImpl.findByTotalAmountLessThanPerYear(year, base, page);
	}

}
