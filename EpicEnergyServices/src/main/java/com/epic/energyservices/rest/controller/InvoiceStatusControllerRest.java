package com.epic.energyservices.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.InvoiceStatus;
import com.epic.energyservices.serviceImpl.InvoiceStatusServiceImpl;

@RestController
@RequestMapping("/rest/invoiceStatus")
public class InvoiceStatusControllerRest {

	@Autowired
	InvoiceStatusServiceImpl invoiceStatusServiceImpl;

//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/addInvoiceStatus")
	public InvoiceStatus addInvoiceStatus(@RequestBody InvoiceStatus invoiceStatus) {
		return invoiceStatusServiceImpl.addInvoiceStatus(invoiceStatus);
	}

}
