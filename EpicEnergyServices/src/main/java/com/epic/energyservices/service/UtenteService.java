package com.epic.energyservices.service;

import com.epic.energyservices.model.Utente;
/**
 * 
 * Service gestione Utente
 *
 */
public interface UtenteService {

	/**
	 * @return Salva nuovo utente.
	 */
	public Utente save(Utente utente);
	/**
	 * @return Modifica un utente.
	 */
	public Utente update(Utente utente, int utenteId);
	/**
	 * @return Elimina un utente.
	 */
	public String delete(int utenteId);
	
}
