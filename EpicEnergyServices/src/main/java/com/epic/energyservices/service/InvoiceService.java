package com.epic.energyservices.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;

import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.model.InvoiceStatus;

/**
 * 
 * Service fattura
 *
 */
public interface InvoiceService {
	/**
	 * 
	 * @param invoice  informazioni della fattura (il numero non deve essere
	 *                 inserito perchè automatico).
	 * @param clientId id del cliente a cui associare la fattura.
	 * @return Salvataggio fattura su DB.
	 */
	public Invoice save(Invoice invoice, int clientId);

	/**
	 * 
	 * @param invoice  informazioni della fattura (il numero deve essere inserito
	 *                 per identificare la fattura da modificare).
	 * @param clientId id del cliente a cui è associata la fattura.
	 * @return Modifica fattura su DB.
	 */
	public Invoice update(Invoice invoice, int clientId);

	/**
	 * 
	 * @param number   numero fattura da eliminare.
	 * @param clientId id del cliente a cui è associata la fattura.
	 * @return Eliminazione dal DB.
	 */
	public String delete(int number, int clientId);

	/**
	 * @return Fatture per cliente ricercato.
	 */
	public Page<Invoice> findByClientId(int id, Pageable page);

	/**
	 * @return Fatturato annuale del cliente per l'anno scelto.
	 */
	public BigDecimal getSum(int clientId, int year);

	/**
	 * @return Fatturato annuale dei clienti per l'anno scelto.
	 */
	public Page<List<Map<Client, BigDecimal>>> getAmountByYear(int year, Pageable page);

	/**
	 * @return Fatture per range importo.
	 */
	public Page<Invoice> findByAmountRange(BigDecimal min, BigDecimal max, Pageable page);

	/**
	 * @return Fatture per range importo per anno scelto.
	 */
	public Page<Invoice> findByAmountRangePerYear(int year, BigDecimal min, BigDecimal max, Pageable page);

	/**
	 * @return Fatture con importi maggiori di un certo valore per l'anno
	 *         selezionato.
	 */
	public Page<Invoice> findByAmountGreaterThanPerYear(int year, BigDecimal base, Pageable page);

	/**
	 * @return Fatture minori di un certo valore per l'anno selezionato.
	 */
	public Page<Invoice> findByAmountLessThanPerYear(int year, BigDecimal base, Pageable page);

	/**
	 * @return Tutte le fatture per l'anno selezionato.
	 */
	public Page<Invoice> findAllInvoiceByYear(int year, Pageable page);

	/**
	 * @return Fatture per stato.
	 */
	public Page<Invoice> findByInvoiceStatusInvoiceStatus(String invoiceStatus, Pageable page);

	/**
	 * @return Fatturato annuale maggiore di un certo valore per anno.
	 */
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountGreaterThanPerYear(int year, BigDecimal base,
			Pageable page);

	/**
	 * @return Fatturato annuale minore di per anno.
	 */
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountLessThanPerYear(int year, BigDecimal base,
			Pageable page);

	/**
	 * @return Fatturato annuale range per anno.
	 */
	public Page<List<Map<Client, BigDecimal>>> findByTotalAmountRangePerYear(int year, BigDecimal min, BigDecimal max,
			Pageable page);

	/**
	 * @return Fattura per numero e cliente.
	 */
	public Invoice findByNumberAndClientId(int number, int clientId);

	/**
	 * @return Lista fattura per cliente id.
	 */
	public List<Invoice> findByClientId(int clientId);

	/**
	 * 
	 * @param id id fattura
	 * @return Fattura per id ricercato.
	 */
	public Invoice findById(int id);

	/**
	 * @return Tutte le fatture.
	 */
	public List<Invoice> findAll();

	/**
	 * 
	 * @param clientId Elimina tutte le fatture apparteneti al cliente inserito.
	 */
	public void deleteAll(int clientId);

	/**
	 * 
	 * @param clientId
	 * @param year
	 * @return Fatture dell'anno e client selezionato
	 */
	public Page<Invoice> getAmountClientPerYear(int clientId, int year, Pageable page);

}
