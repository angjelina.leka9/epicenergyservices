package com.epic.energyservices.service;

import com.epic.energyservices.model.BusinessType;
/**
 * 
 * Gestione Tipi di Client
 *
 */
public interface BusinessTypeService {
	
	/**
	 *
	 * Aggiunge tipi di Client al DB
	 * Devono essere preinseriti nel DB.
	 * Quando viene creato un cliente, si sceglie tra i tipi disponibili
	 *
	 */
	public BusinessType addBusinessType (BusinessType businessType);
	public BusinessType update (BusinessType businessType, int id);
}
