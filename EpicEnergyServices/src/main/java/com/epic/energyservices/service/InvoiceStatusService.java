package com.epic.energyservices.service;

import com.epic.energyservices.model.InvoiceStatus;
/**
 * 
 * Service stato fattura.
 *
 */
public interface InvoiceStatusService {
	/**
	 * 
	 * @param invoiceStatus.
	 * @return Ritorna id dello stato.
	 */
	public int invoiceStatusId (String invoiceStatus);
	/**
	 * 
	 * @param invoiceStatus.
	 * @return Salva su DB gli stati.
	 * Devono essere preinseriti nel DB.
	 * Quando viene creata una fattura, si sceglie tra gli stati disponibili
	 */
	public InvoiceStatus addInvoiceStatus (InvoiceStatus invoiceStatus);
}
