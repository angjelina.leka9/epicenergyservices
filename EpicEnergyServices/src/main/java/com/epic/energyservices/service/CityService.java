package com.epic.energyservices.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.epic.energyservices.model.City;
import com.epic.energyservices.model.Province;

/**
 * 
 * Service City
 *
 */
public interface CityService {
	/**
	 * 
	 * @param city.
	 * @return aggiunge una citta.
	 */
	City add(City city);

	/**
	 * 
	 * @param cities.
	 * @return aggiunge un elenco di citta.
	 */
	int saveAll(List<City> cities);

	/**
	 * 
	 * @return ritorna tutte le citta.
	 */
	List<City> getAllCities();

	/**
	 * 
	 * @param id.
	 * @return Trova citta per id.
	 */
	Optional<City> getById(int id);

	/**
	 * 
	 * @param name.
	 * @param acronym sigla/acronimo.
	 * @return Recupera una città con nome e provincia.
	 */
	Optional<City> getByName(String name, String acronym);

	/**
	 * 
	 * @param acronym.
	 * @param pageable.
	 * @return Recupera tutte le città di una provincia.
	 */
	Page<City> getByProvince(String acronym, Pageable pageable);

	/**
	 * 
	 * @param pageable
	 * @return Recupera l'elenco delle province
	 */
	Page<Province> getProvinces(Pageable pageable);

	/**
	 * 
	 * @param name
	 * @return Cerca la provincia in base al nome del comune inserito.
	 */
	Province findProvinceByCityName(String name);

	/**
	 * 
	 * @param name
	 * @return Ritorna id della provincia in base al comune inserito.
	 */
	int findProvinceIdByCityName(String name);
	/**
	 * 
	 * @param name
	 * @return Cerca il comune per nome.
	 */
	City findByName(String name);

}
