package com.epic.energyservices.service;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.epic.energyservices.model.Client;

public interface ClientService {
	/**
	 * 
	 * @param client informazioni del cliente.
	 * @return Salvataggio su DB.
	 */
	public Client save(Client client);

	/**
	 * 
	 * @param client   informazioni da modificare del cliente.
	 * @param clientId da modificare.
	 * @return Modifica su DB.
	 */
	public Client update(Client client, int clientId);

	/**
	 * 
	 * @param clientId id del cliente da eliminare.
	 * @return Eliminazione da DB.
	 */
	public String delete(int clientId);

	/**
	 * @return Filtra per parte del nome.
	 */
	public Page<Client> filterByBusinessName(String businessName, Pageable page);

	/**
	 * @return Ritorna data ultimo contatto per il cliente selezionato.
	 */
	public Date getLastContact(int clientId);

	/**
	 * @return Ritorna data di registrazione del cliente selezionato.
	 */
	public Date getCreatedAt(int clientId);

	/**
	 * @return Ordina per ragione sociale, ordine alfabetico.
	 */
	public Page<Client> orderByBusinessName(Pageable page);

	/**
	 * @return Ordina per ultimo contatto <Client.toString(), Date>
	 */
	public List<Entry<String, Date>> orderByLastContact();

	/**
	 * @return Ordina per data di inserimento, ordine crescente.
	 */
	public Page<Client> orderByCreatedAt(Pageable page);

	/**
	 * @return Ordina per provincia, ordine alfabetico.
	 */
	public Page<Client> orderByRegisteredOffice(Pageable page);

	// **GESTIONE PARTITA IVA**//
	/**
	 * @return 7 numeri random per la prima parte della partita iva. In realtà non
	 *         dovrebbe essere casuale ma autonomamente ogni provincia incrementa di
	 *         un unità quando si registra una nuova p.iva.
	 */
	public int randomNumber();

	/**
	 * @param provinceId
	 * @return Dall'id della provincia creo il codice di 3 cifre che rappresenta la
	 *         provincia. Le province più grandi hanno più di un codice, ma per
	 *         semplificare ho usare l'id.
	 */
	public String provinceCode(int provinceId);

	/**
	 * 
	 * @param randomNumber 7 numeri generati casualmente.
	 * @param provinceCode codice della provincia.
	 * @return Partita iva calcolata in base alla Formula di Luhn.
	 * 
	 */
	public String partitaIvaCalc(int randomNumber, String provinceCode);

	/**
	 * @return Ritorna tutti i clienti.
	 */
	public Page<Client> findAllClientsPageSort(Integer page, Integer size, String dir, String sort);

	/**
	 * @return Filtra per data registrazione.
	 */
	public Page<Client> filterByCreatedAt(Date startDate, Date endDate, Pageable page);

	/**
	 * @return Filtra per provincia.
	 */
	public Page<Client> filterByProvince(String province, Pageable page);

	/**
	 * @return Ritorna clienti con stesso tipo.
	 */
	public Page<Client> findByBusinessTypeBusinessType(String businessType, Pageable page);

	/**
	 * @return Per impaginare.
	 */
	public Page<Client> findPaginated(int pageNumber, int pageSize, String sortField, String sortDirection);

	/**
	 * 
	 * @return Tutti i clienti.
	 */
	public List<Client> findAll();

	/**
	 * 
	 * @param clientId
	 * @return Cliente per id.
	 */
	public Client findById(int clientId);

}
