package com.epic.energyservices.service;

import java.util.List;

import com.epic.energyservices.model.OperationalHeadquarters;

/**
 * 
 * Service gestione Sede Operativa.
 *
 */
public interface OperationalHeadquartersService {
	/**
	 * 
	 * @param operationalHeadquarters informazioni sede operativa
	 * @param clientId                id del cliente a cui associare la sede.
	 *                                operativa.
	 * @return Salvataggio nel DB.
	 */
	public OperationalHeadquarters save(OperationalHeadquarters operationalHeadquarters, int clientId);
	/**
	 * 
	 * @param operationalHeadquarters informazioni sede operativa.
	 * @param id               id della sede operativa.
	 * @return Modifica nel DB.
	 */
	public OperationalHeadquarters update(OperationalHeadquarters operationalHeadquarters, int id);
	/**
	 * 
	 * @param id              id della sede operativa.
	 * @return Eliminazione dal DB.
	 */
	public String delete(int id);
	/**
	 * 
	 * @param id id del client.
	 * @return Ricerca per id del client.
	 */
	public OperationalHeadquarters findByClientId(int id);
	/**
	 * 
	 * @param province stringa nome completo o parte del nome.
	 * @return Ricerca per parte del nome della provincia.
	 */
	public OperationalHeadquarters filterByProvince(String province);
	/**
	 * 
	 * @param id id della sede operativa.
	 * @return Sede operativa ricercata.
	 */
	public OperationalHeadquarters findById(int id);
	/**
	 * 
	 * @return Tutte le sedi Operative registrate.
	 */
	public List<OperationalHeadquarters> findAll();

}
