package com.epic.energyservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.InvoiceStatus;
/**
 * 
 * Repository InvoiceStatus - Stato della fattura
 *
 */
public interface InvoiceStatusRepository extends JpaRepository<InvoiceStatus, Integer> {
	/**
	 * @return Stato della fattura uguale alla stringa inserita se presente nel DB.
	 */
	public InvoiceStatus findByInvoiceStatus(String stato);
	
}
