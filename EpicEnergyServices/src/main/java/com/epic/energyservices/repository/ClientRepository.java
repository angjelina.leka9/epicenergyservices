package com.epic.energyservices.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.model.City;
import com.epic.energyservices.model.Client;

/**
 * 
 * Repository Client.
 *
 */
public interface ClientRepository extends JpaRepository<Client, Integer> {

	/**
	 * @param businessName = ragione sociale cioè nome azienda.
	 * @param page         = numero della pagina da consultare.
	 * @return Aziende con lo stesso nome.
	 */
	public Page<Client> findByBusinessName(String businessName, Pageable page);

	/**
	 * @param businessType = tipo di azienda.
	 * @param page         = numero della pagina da consultare.
	 * @return Aziende con lo stesso tipo.
	 */
	public Page<Client> findByBusinessType(BusinessType businessType, Pageable page);

	/**
	 * @param businessType = Stringa che indica tipo di azienda.
	 * @param page         = numero della pagina da consultare.
	 * @return Aziende con lo stesso tipo.
	 */
	public Page<Client> findByBusinessTypeBusinessType(String businessType, Pageable page);// rendere maiuscolo

	/**
	 * @return ordina per ragione sociale.
	 */
	@Query(name = "orderByBusinessName")
	public Page<Client> orderByBusinessName(Pageable page);

	/**
	 * @return ordina per ultimo contatto.
	 */
	@Query(name = "orderByCreatedAt")
	public Page<Client> orderByCreatedAt(Pageable page);

	/**
	 * @return ordina per provincia.
	 */
	@Query(name = "orderByRegisteredOffice")
	public Page<Client> orderByRegisteredOffice(Pageable page);

	/**
	 * @return filtra per parte del nome.
	 */
	@Query(name = "filterByBusinessName")
	public Page<Client> filterByBusinessName(String businessName, Pageable page);

	/**
	 * @return data di registrazione del cliente selezionato.
	 */
	@Query(name = "getCreatedAt")
	public Date getCreatedAt(int clientId);

	/**
	 * @return filtra per parte della provincia.
	 */
	@Query(name = "filterByProvince")
	public Page<Client> filterByProvince(String province, Pageable page);

	/**
	 * @return data di registrazione range.
	 */
	@Query(name = "filterByCreatedAt")
	public Page<Client> filterByCreatedAt(Date startDate, Date endDate, Pageable page);

	/**
	 * 
	 * @param partitaIva
	 * @return Client per partita iva.
	 */
	public Optional<City> findByPartitaIva(String partitaIva);

}
