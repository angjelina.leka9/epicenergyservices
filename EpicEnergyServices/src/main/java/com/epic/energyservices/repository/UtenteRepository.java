package com.epic.energyservices.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.Utente;

/**
 * 
 * Repository utente.
 *
 */
public interface UtenteRepository extends JpaRepository<Utente, Integer>{
	/**
	 * @return Utente in base all'id inserito.
	 */
	public Optional<Utente> findById(int userId);
	/**
	 * @return Utenti in base al nome inserito.
	 */
	public Page<Utente> findByName(String name, Pageable page);
	/**
	 * @return Utenti in base al cognome inserito.
	 */
	public Page<Utente> findBySurname(String surname, Pageable page);
	/**
	 * @return Utente in base all'email inserita.
	 */
	public Optional<Utente> findByEmail(String email);
	/**
	 * @return Tutti gli utenti.
	 */
	public Page<Utente> findAll(Pageable page);
}
