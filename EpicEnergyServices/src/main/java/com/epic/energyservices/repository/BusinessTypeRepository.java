package com.epic.energyservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.BusinessType;

/**
 * 
 * Repository Tipo di Client
 *
 */
public interface BusinessTypeRepository extends JpaRepository<BusinessType, Integer> {

	/**
	 * @return trova BusinessType corrispondente alla stringa inserita
	 */
	public BusinessType findByBusinessType(String businessType);


}
