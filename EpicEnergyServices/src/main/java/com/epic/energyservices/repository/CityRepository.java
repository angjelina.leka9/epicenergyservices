package com.epic.energyservices.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.City;

/**
 * 
 * Repository City.
 *
 */
public interface CityRepository extends JpaRepository<City, Integer> {

	/**
	 * 
	 * @return Comune ricercato per nome e provincia.
	 */
	Optional<City> findByNameAndProvinceAcronym(String name, String acronym);

	/**
	 * @param acronimo/sigla provincia.
	 * @return Comuni della provincia inserita.
	 */
	Page<City> findAllByProvinceAcronym(String acronym, Pageable pageable);

	/**
	 * 
	 * @return Ricerca per nome.
	 */
	Optional<City> findByName(String name);
}
