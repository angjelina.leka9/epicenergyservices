package com.epic.energyservices.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Invoice;
/**
 * 
 * Repository Invoice fattura.
 *
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Integer>{

	/**
	 * @return Fatture per cliente.
	 */
	public Page<Invoice> findByClientId(int id, Pageable page);
	
	
	/**
	 * @return Fatturato annuale del cliente per l'anno scelto.
	 */
	@Query(name = "getSum") 
	public BigDecimal getSum(int clientId, int year);
	
	/**
	 * @return Fatturato annuale del cliente per l'anno scelto.
	 */
	@Query(name = "getAmountClientPerYear") 
	public Page<Invoice> getAmountClientPerYear(int clientId, int year,Pageable page);
	

	/**
	 * @return Fatturato annuale dei clienti per l'anno scelto.
	 */
	@Query(name = "getAmountByYear") 
	public Page<List<Map<Client , BigDecimal>>> getAmountByYear(int year, Pageable page);
	

	/**
	 * @return Fatture per range importo.
	 */
	@Query(name = "findByAmountRange")
	public List<Invoice> findByAmountRange(BigDecimal min, BigDecimal max);
	
	
	/**
	 * @return Fatture per range importo per anno scelto.
	 */
	@Query(name = "findByAmountRangePerYear")
	public Page<Invoice> findByAmountRangePerYear(int year, BigDecimal min, BigDecimal max, Pageable page);
	
	
	/**
	 * @return Fatture con importi maggiori di un certo valore per l'anno selezionato.
	 */
	@Query(name = "findByAmountGreaterThanPerYear")
	public Page<Invoice> findByAmountGreaterThanPerYear(int year, BigDecimal base, Pageable page);
	
	
	/**
	 * @return Importi minori di un certo valore per l'anno selezionato.
	 */
	@Query(name = "findByAmountLessThanPerYear")
	public Page<Invoice> findByAmountLessThanPerYear(int year, BigDecimal base, Pageable page);
	
	/**
	 * @return Tutte le fatture per l'anno selezionato.
	 */
	@Query(name = "findAllInvoiceByYear")
	public Page<Invoice> findAllInvoiceByYear(int year, Pageable page);
	
	
	/**
	 * @return Fatture per stato.
	 */
	public Page<Invoice> findByInvoiceStatusInvoiceStatus(String invoiceStatus, Pageable page);
	
	
	/**
	 * @return Fatturato annuale maggiore di un certo valore per l'anno selezionato.
	 */
	@Query(name = "findByTotalAmountGreaterThanPerYear")
	public Page<List<Map<Client , BigDecimal>>> findByTotalAmountGreaterThanPerYear(int year, BigDecimal base, Pageable page);
	
	
	/**
	 * @return Fatturato annuale minore di un certo valore per l'anno selezionato.
	 */
	@Query(name = "findByTotalAmountLessThanPerYear")
	public Page<List<Map<Client , BigDecimal>>> findByTotalAmountLessThanPerYear(int year, BigDecimal base, Pageable page);
	
	/**
	 * @return Fatturato annuale range per anno selezionato.
	 */
	@Query(name = "findByTotalAmountRangePerYear")
	public Page<List<Map<Client , BigDecimal>>> findByTotalAmountRangePerYear(int year, BigDecimal min, BigDecimal max, Pageable page);

	
	/**
	 * @return Fattura per numero e cliente.
	 */
	public Invoice findByNumberAndClientId(int number, int clientId);
	
	/**
	 * @return Lista fattura per cliente id.
	 */
	public List<Invoice> findByClientId(int id);
	
	
	/**
	 * @return Fattura per id e per cliente id
	 */
	public Invoice findByIdAndClientId(int id, int clientId);
	
	
	/**
	 * @return Fatturato annuale maggiore di per anno TEST JUNIT.
	 */
	@Query(name = "findByTotalAmountGreaterThanPerYear")
	public List<Map<Client , BigDecimal>> findByTotalAmountGreaterThanPerYear(int year, BigDecimal base);

	/**
	 * @return fatturato annuale dei clienti per l'anno scelto TEST JUNIT.
	 */
	@Query(name = "getAmountByYear") 
	public List<Map<Client , BigDecimal>> getAmountByYear(int year);

}
