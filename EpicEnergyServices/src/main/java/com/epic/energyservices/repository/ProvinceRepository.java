package com.epic.energyservices.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.Province;

/**
 * 
 * Repository Provincia.
 *
 */

public interface ProvinceRepository extends JpaRepository<Province, Integer> {

	/**
	 * 
	 * @param acronym
	 * @return Recupera la provincia tramite la sigla/acronimo.
	 */
	Optional<Province> findByAcronym(String acronym);
}
