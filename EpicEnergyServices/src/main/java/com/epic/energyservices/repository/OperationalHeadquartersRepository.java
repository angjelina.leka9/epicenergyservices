package com.epic.energyservices.repository;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.epic.energyservices.model.OperationalHeadquarters;
/**
 * 
 * Repository OperationalHeadquarters - Sede Operativa.
 *
 */
public interface OperationalHeadquartersRepository extends JpaRepository<OperationalHeadquarters, Integer>{
	/**
	 * 
	 * @param id del clien.
	 * @return Sede operativa per cliente inserito.
	 */
	public OperationalHeadquarters findByClientId (int id);
	/**
	 * 
	 * @param province
	 * @return Sedi operativa per parte del nome della provincia inserita.
	 */
	@Query(name="filterByProvince")
	public OperationalHeadquarters filterByProvince(String province);

	
}



