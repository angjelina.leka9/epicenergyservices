package com.epic.energyservices.security;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.epic.energyservices.model.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "role")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BaseEntity {

	@Enumerated(EnumType.STRING)
	private RoleType roleType;

	@Override
	public String toString() {
		return String.format("Role: id%d, roletype%s ", roleType);

	}

}