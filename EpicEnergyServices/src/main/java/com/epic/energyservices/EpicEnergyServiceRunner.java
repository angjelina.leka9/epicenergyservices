package com.epic.energyservices;

import java.io.FileInputStream;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.model.City;
import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Contact;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.model.InvoiceStatus;
import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.model.Province;
import com.epic.energyservices.model.RegisteredOffice;
import com.epic.energyservices.model.Utente;
import com.epic.energyservices.repository.BusinessTypeRepository;
import com.epic.energyservices.serviceImpl.BusinessTypeServiceImpl;
import com.epic.energyservices.serviceImpl.CityServiceImpl;
import com.epic.energyservices.serviceImpl.ClientServiceImpl;
import com.epic.energyservices.serviceImpl.InvoiceServiceImpl;
import com.epic.energyservices.serviceImpl.UtenteServiceImpl;

import be06.epicode.cities.CitiesLoader;



@Component
public class EpicEnergyServiceRunner implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(EpicEnergyServiceRunner.class);

	@Autowired
	ClientServiceImpl clientServiceImpl;

	@Autowired
	CityServiceImpl cityServiceImpl;
	
	@Autowired
	InvoiceServiceImpl invoiceServiceImpl;

	@Autowired 
	UtenteServiceImpl utenteServiceImpl;

	@Autowired
	BusinessTypeRepository businessTypeRepository;
	
	@Autowired
	BusinessTypeServiceImpl businessTypeServiceImpl;
	 @Override
	public void run(String... args) throws Exception {
    
		Client c = Client.builder()
			.businessName("qwrgqe3")
				.email("qerbgqbazienda.com")
				.pec("ergqeqr@pec.it")
				.phone(333333333L)
				.contact(Contact.builder()
						.contactName("kfnverj Nome")
						.contactSurname("qergq Cognome")
						.contactEmail("3g34g@contatto.com")
						.contactPhone(147258369L)
						.build())
				.registeredOffice(RegisteredOffice.builder()
						.adress("via wdtf4")
						.streetNumber("18")
						.locality("località 234ft")
						.postalCode(25930)
						.city(City.builder()
								.name("Zoagli")
								.build())
						.build())
			.businessType(BusinessType.builder().businessType("SPA").build())
				.build();
		
			//clientServiceImpl.save(c);
		 	//clientServiceImpl.update(c, 6);	
			//clientServiceImpl.delete(25);
				
	
		
		OperationalHeadquarters oh = OperationalHeadquarters.builder()
				.adress("modifica")
				.streetNumber("12")
				.locality("frazione mouse")
				.postalCode(27326)
				.city(City.builder()
						.name("Bari").build())
				.build();
		

	//	operationalHeadquartersServiceImpl.save(oh, 23);
	//	operationalHeadquartersServiceImpl.update(oh, 4);
	//	operationalHeadquartersServiceImpl.delete(id);
		
		
		
		Invoice f = Invoice.builder()
				.number(1)
				.amount(BigDecimal.valueOf(10000.02))
				.invoiceStatus(InvoiceStatus.builder()
						.invoiceStatus("Paid")
						.build())
				.build();
		
		//invoiceServiceImpl.save(f, 16);
		//invoiceServiceImpl.update(f, 6);
		//invoiceServiceImpl.delete(2, 1);
		
		
		Utente u = Utente.builder().email("fiona@email.com")
				.name("Fiona")
				.surname("Fleisch")
				.username("fionafiona")
				.password("fiona9")
				.build();
		
		//utenteServiceImpl.save(u);
		
		//businessTypeServiceImpl.addBusinessType(BusinessType.builder().businessType("paid").build());
		
		
		log.info("PIVA generata: {}", clientServiceImpl.partitaIvaCalc(clientServiceImpl.randomNumber(), "023"));
		
		
		log.info("ID della provincia: {}",cityServiceImpl.findProvinceIdByCityName("Torino"));
		
		log.info("Nome city : {}", clientServiceImpl.findById(1).getRegisteredOffice().getCity().getName());
		
		log.info("Codice provincia da int a stringa : {}", clientServiceImpl.provinceCode(7));
		
		
		log.info("findByInvoiceStatusInvoiceStatus : {}", invoiceServiceImpl.findByInvoiceStatusInvoiceStatus("emessa", Pageable.ofSize(1)));
		
		
		// CARICAMENTO DB
		
//		
//		  if (cityServiceImpl.getProvinces(Pageable.unpaged()).isEmpty()) {
//		  //E:\\Codice\\Epicode\\BE06\\CitiesLoader\\comuni.csv 
//		  var fileName = "C:\\Users\\39333\\eclipse-workspace\\workspaceEclipse\\CitiesLoader\\comuni.csv";
//		  // apro uno stream sulla url e richiamo il metodo di caricamento 
//		  var cities =  CitiesLoader.load(new FileInputStream(fileName));
//		  cities.stream().map(city -> City.builder().capital(city.isCapital())
//				  .name(city.getName()) 
//				  .province(Province.builder()
//						  .name(city.getProvince() 
//								  .getName()) 
//						  .acronym(city.getProvince()
//								  .getAcronym())
//						  .build()) 
//				  .build()) 
//		  .forEach(city -> cityServiceImpl.add(city));
//		  
//		  log.info("Loaded {} cities", cities.size());
//		  
//		  }
//		  
//		  log.info("Tutte le città  sono state salvate sul db");

	}
}
