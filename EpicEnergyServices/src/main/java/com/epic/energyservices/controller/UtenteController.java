package com.epic.energyservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epic.energyservices.model.Utente;
import com.epic.energyservices.serviceImpl.UtenteServiceImpl;


@CrossOrigin(origins = "http://domain2.com", maxAge=3600)
@RestController
@RequestMapping("/utente")
public class UtenteController {
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	UtenteServiceImpl utenteServiceImpl;
	
	@PostMapping("/save")
	public Utente save(@RequestBody Utente utente) {
		return utenteServiceImpl.save(utente);
	}

}
