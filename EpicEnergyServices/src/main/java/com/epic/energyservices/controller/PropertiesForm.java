package com.epic.energyservices.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.epic.energyservices.model.Client;

public class PropertiesForm {
	private List<Map<Client, BigDecimal>> properties = new ArrayList<>();

    public List<Map<Client, BigDecimal>> getProperties() {
        return properties;
    }

    public void setProperties(List<Map<Client, BigDecimal>> list) {
        this.properties = list;
    }
}