package com.epic.energyservices;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.AfterTestExecution;

import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.model.City;
import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Contact;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.model.InvoiceStatus;
import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.model.RegisteredOffice;
import com.epic.energyservices.model.Utente;
import com.epic.energyservices.repository.ClientRepository;
import com.epic.energyservices.serviceImpl.ClientServiceImpl;

@SpringBootTest
class ClientTest {
	
	@Autowired
	ClientServiceImpl clientServiceImpl;

	Client c = Client.builder()
			.businessName("JUnitTest")
			.email("email@mail.com")
			.pec("email@pec.it")
			.phone(333333333L)
			.contact(Contact.builder()
					.contactName("contact Nome")
					.contactSurname("contact Cognome")
					.contactEmail("email@contatto.com")
					.contactPhone(147258369L)
					.build())
			.registeredOffice(RegisteredOffice.builder()
					.adress("via testJunit")
					.streetNumber("18")
					.locality("località JUnit")
					.postalCode(25930)
					.city(City.builder()
							.name("Ventimiglia")
							.build())
					.build())
			.businessType(BusinessType.builder().businessType("SPA").build())
			.build();
	
		
	
	@Test
	@DisplayName("save(Client client): controllo salvataggio nuovo cliente")
	void saveClientTest() {
		int clients = clientServiceImpl.findAll().size();
		clientServiceImpl.save(c);
		assertEquals(clients + 1, clientServiceImpl.findAll().size(), "Valore atteso: "+(clients + 1));
	}
	
	
	@Test
	@DisplayName("update(Client client): controllo modifica cliente")
	void updateClientTest() {
		int clients = clientServiceImpl.findAll().size();
		int id= clientServiceImpl.findAll().get(clients-1).getId();
		Client clientTest = Client.builder().businessName("ModificaJUnit").build();
		clientServiceImpl.update(clientTest, id);
		assertEquals("ModificaJUnit", clientServiceImpl.findById(id).getBusinessName(), "Valore atteso: ModificaJUnit");
	}
	
	@Test
	@DisplayName("delete(int id): controllo eliminazione cliente")
	void deleteClientTest() {
		int clients = clientServiceImpl.findAll().size();
		int id = clientServiceImpl.findAll().get(clients-1).getId();
		clientServiceImpl.delete(id);
		assertEquals(clients - 1, clientServiceImpl.findAll().size(), "Valore atteso: "+(clients - 1));
	}
}
