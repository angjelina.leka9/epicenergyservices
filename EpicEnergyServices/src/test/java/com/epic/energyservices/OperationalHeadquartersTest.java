package com.epic.energyservices;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.event.annotation.AfterTestExecution;

import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.model.City;
import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Contact;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.model.InvoiceStatus;
import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.model.RegisteredOffice;
import com.epic.energyservices.model.Utente;
import com.epic.energyservices.repository.ClientRepository;
import com.epic.energyservices.serviceImpl.ClientServiceImpl;
import com.epic.energyservices.serviceImpl.OperationalHeadquartersServiceImpl;

@SpringBootTest
class OperationalHeadquartersTest {
	
	@Autowired
	OperationalHeadquartersServiceImpl operationalHeadquartersServiceImpl;
	
	@Autowired
	ClientServiceImpl clientServiceImpl;

	OperationalHeadquarters oh = OperationalHeadquarters.builder()
			.adress("provaJUnit")
			.streetNumber("12")
			.locality("frazione")
			.postalCode(27326)
			.city(City.builder()
					.name("Milano").build())
			.build();
	
	Client c = Client.builder()
			.businessName("JUnitTest")
			.email("email@mail.com")
			.pec("email@pec.it")
			.phone(333333333L)
			.contact(Contact.builder()
					.contactName("contact Nome")
					.contactSurname("contact Cognome")
					.contactEmail("email@contatto.com")
					.contactPhone(147258369L)
					.build())
			.registeredOffice(RegisteredOffice.builder()
					.adress("via testJunit")
					.streetNumber("18")
					.locality("località JUnit")
					.postalCode(25930)
					.city(City.builder()
							.name("Ventimiglia")
							.build())
					.build())
			.businessType(BusinessType.builder().businessType("SPA").build())
			.build();
		
	
	@Test
	@DisplayName("save(OperationalHeadquarters operationalHeadquarters, int clientId): controllo salvataggio sede operativa")
	void saveOperationalHeadquartersTest() {
		clientServiceImpl.save(c);
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients-1).getId();
		int size = operationalHeadquartersServiceImpl.findAll().size();
		operationalHeadquartersServiceImpl.save(oh, clientId);
		assertEquals(size + 1, operationalHeadquartersServiceImpl.findAll().size(), "Valore atteso: provaJUnit");
	}
	
	@Test
	@DisplayName("update(int id): controllo modifica sede operativa")
	void updateOperationalHeadquartersTest() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients-1).getId();
		int id = operationalHeadquartersServiceImpl.findByClientId(clientId).getId();
		OperationalHeadquarters operationalHeadquartersTest = OperationalHeadquarters.builder().locality("ModificaJUnit").build();
		operationalHeadquartersServiceImpl.update(operationalHeadquartersTest, id);
		assertEquals("ModificaJUnit", operationalHeadquartersServiceImpl.findById(id).getLocality(), "Valore atteso: ModificaJUnit");
	}
	
	@Test
	@DisplayName("delete(int id): controllo eliminazione sede operativa")
	void deleteOperationalHeadquartersTest() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients-1).getId();
		int id = operationalHeadquartersServiceImpl.findByClientId(clientId).getId();
		operationalHeadquartersServiceImpl.delete(id);
		assertEquals(null,  operationalHeadquartersServiceImpl.findByClientId(clientId), "Valore atteso: null");
		clientServiceImpl.delete(clientId);
	}

	

}
