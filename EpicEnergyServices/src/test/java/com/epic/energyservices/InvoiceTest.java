package com.epic.energyservices;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.event.annotation.AfterTestExecution;

import com.epic.energyservices.model.BusinessType;
import com.epic.energyservices.model.City;
import com.epic.energyservices.model.Client;
import com.epic.energyservices.model.Contact;
import com.epic.energyservices.model.Invoice;
import com.epic.energyservices.model.InvoiceStatus;
import com.epic.energyservices.model.OperationalHeadquarters;
import com.epic.energyservices.model.RegisteredOffice;
import com.epic.energyservices.model.Utente;
import com.epic.energyservices.repository.ClientRepository;
import com.epic.energyservices.repository.InvoiceRepository;
import com.epic.energyservices.serviceImpl.ClientServiceImpl;
import com.epic.energyservices.serviceImpl.InvoiceServiceImpl;

@SpringBootTest
class InvoiceTest {

	@Autowired
	ClientServiceImpl clientServiceImpl;

	@Autowired
	InvoiceServiceImpl invoiceServiceImpl;
	
	@Autowired
	InvoiceRepository invoiceRepository;

	Invoice i = Invoice.builder().amount(BigDecimal.valueOf(450.55))
			.invoiceStatus(InvoiceStatus.builder().invoiceStatus("Paid").build()).build();
	Invoice ii = Invoice.builder().amount(BigDecimal.valueOf(50.60))
			.invoiceStatus(InvoiceStatus.builder().invoiceStatus("Paid").build()).build();
	

	Client c = Client.builder().businessName("JUnitTest").email("email@mail.com").pec("email@pec.it").phone(333333333L)
			.contact(Contact.builder().contactName("contact Nome").contactSurname("contact Cognome")
					.contactEmail("email@contatto.com").contactPhone(147258369L).build())
			.registeredOffice(
					RegisteredOffice.builder().adress("via testJunit").streetNumber("18").locality("località JUnit")
							.postalCode(25930).city(City.builder().name("Ventimiglia").build()).build())
			.businessType(BusinessType.builder().businessType("SPA").build()).build();

	
	@Test
	@DisplayName("save(Invoice invoice, int clintId): controllo salvataggio fattura")
	void saveInvoiceTest() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients - 1).getId();
		int invoices = invoiceServiceImpl.findByClientId(clientId).size();
		invoiceServiceImpl.save(i, clientId);
		assertEquals(invoices + 1, invoiceServiceImpl.findByClientId(clientId).size(),
				"Valore atteso: " + (invoices + 1));
	}

	@Test
	@DisplayName("update(Invoice invoice, int clientId): controllo modifica fattura")
	void updateInvoiceTest() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients - 1).getId();
		invoiceServiceImpl.save(i, clientId);
		int invoices = invoiceServiceImpl.findByClientId(clientId).size();
		int number = invoiceServiceImpl.findByClientId(clientId).get(invoices - 1).getNumber();
		Invoice invoiceTest = Invoice.builder().number(number).amount(BigDecimal.valueOf(10.02))
				.invoiceStatus(InvoiceStatus.builder().invoiceStatus("Paid").build()).build();
		invoiceServiceImpl.update(invoiceTest, clientId);
		assertEquals("Paid", invoiceTest.getInvoiceStatus().getInvoiceStatus(), "Valore atteso: Paid");
		assertEquals(BigDecimal.valueOf(10.02),
				invoiceServiceImpl.findByNumberAndClientId(number, clientId).getAmount(), "Valore atteso: 10.02");
	}

	@Test
	@DisplayName("delete(int number, int clientId): controllo eliminazione della fattura")
	void deleteInvoiceTest() {
		int clientId = clientServiceImpl.findAll().get(clientServiceImpl.findAll().size() - 1).getId();
		invoiceServiceImpl.save(i, clientId);
		int invoices = invoiceServiceImpl.findByClientId(clientId).size();
		int number = invoiceServiceImpl.findByClientId(clientId).get(invoices - 1).getNumber();
		invoiceServiceImpl.delete(number, clientId);
		assertEquals(invoices - 1, invoiceServiceImpl.findByClientId(clientId).size(),
				"Valore atteso: " + (invoices - 1));
	}

	@Test
	@DisplayName("getSum(int clientId, int year): fatturato toale per cliente ricercato")
	void getSum() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients - 1).getId();
		invoiceServiceImpl.deleteAll(clientId);
		invoiceServiceImpl.save(i, clientId);
		invoiceServiceImpl.save(ii, clientId);
		assertEquals(BigDecimal.valueOf(501.15), invoiceServiceImpl.getSum(clientId, 2022),
				"Valore atteso: 501.15");
	}
	
	@Test
	@DisplayName("findByAmountRange(BigDecimal min,BigDecimal max): range importi fatture")
	void findByAmountRange() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients - 1).getId();
		invoiceServiceImpl.save(Invoice.builder().amount(BigDecimal.valueOf(1.55))
				.invoiceStatus(InvoiceStatus.builder().invoiceStatus("Paid").build()).build(), clientId);
		assertEquals(1, invoiceRepository.findByAmountRange(BigDecimal.valueOf(0.55), BigDecimal.valueOf(2.55)).size(),
				"valore atteso: 1");
	}
	
	@Test
	@DisplayName("findByTotalAmountGreaterThanPerYear(int year, BigDecimal min, BigDecimal max): range importi fatture per anno")
	void findByTotalAmountGreaterThanPerYear() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients - 1).getId();
		invoiceServiceImpl.save(Invoice.builder().amount(BigDecimal.valueOf(9999999.99))
				.invoiceStatus(InvoiceStatus.builder().invoiceStatus("Paid").build()).build(), clientId);
		assertEquals(1, invoiceRepository.findByTotalAmountGreaterThanPerYear(2022, BigDecimal.valueOf(9999999.50)).size(),
				"valore atteso: 1");
	}
	
	@Test
	@DisplayName("getAmountByYear(int year): lista fatturato annuale anno scelto ordine crescente")
	void getAmountByYear() {
		int clients = clientServiceImpl.findAll().size();
		int clientId = clientServiceImpl.findAll().get(clients - 1).getId();
		invoiceServiceImpl.deleteAll(clientId);
		invoiceServiceImpl.save(Invoice.builder().amount(BigDecimal.valueOf(0.35))
				.invoiceStatus(InvoiceStatus.builder().invoiceStatus("Paid").build()).build(), clientId);
		assertEquals(true, invoiceRepository.getAmountByYear(2022).get(0).containsValue(BigDecimal.valueOf(0.35)),
				"valore atteso: true");
	}
	
	

}
